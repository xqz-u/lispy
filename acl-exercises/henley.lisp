;;;; ex 8.5 https://courses.cs.northwestern.edu/325/exercises/graham-exs.php
(in-package #:henley)

(defparameter *words* (make-hash-table :size 10000))

(defconstant maxword 100)

(defun punc (c)
  (case c
    (#\. '|.|)
    (#\, '|,|)
    (#\; '|;|)
    (#\! '|!|)
    (#\? '|?|)))

(defun make-see () ; refactor
  (let ((prev '|.|))
    #'(lambda (symb)
        (let ((pair (assoc symb (gethash prev *words*))))
          (if (null pair)
              (push (cons symb 1) (gethash prev *words*))
              (incf (cdr pair)))
          (setf prev symb)))))

(defun make-word-register () ; refactor
  (let ((see (make-see)))
    #'(lambda (word-buffer &optional pos)
        (if pos
            ;; when an optional position argument is passed, a sub-sequence of
            ;; `word-buffer' from 0 to `pos' is registered, and the position of
            ;; the index in `word-buffer' is reset and returned
            (prog1
                0
              (funcall see (intern (string-downcase
                                    (subseq word-buffer 0 pos)))))
            (funcall see word-buffer)))))

(defun read-stream (stream func) ; refactor
  (let ((buffer (make-string maxword))
        (pos 0))
    (do ((c (read-char stream nil :eof)
            (read-char stream nil :eof)))
        ((eql c :eof))
      ;; copy a word into the buffer while punctuation/alphabetic chars are found
      (cond ((or (alpha-char-p c) (char= c #\'))
             (progn
               (setf (aref buffer pos) c)
               (incf pos)))
            ;; otherwise, call `func' on each word and reset `pos' according to
            ;; `func'
            ((/= pos 0)
             (setf pos (funcall func buffer pos))))
      ;; call `func' on punctuation signs too; no resetting of `pos' needed here
      (let ((p (punc c)))
        (when p
          (funcall func p))))))

(defun read-text (pathname) ; refactor
  (with-open-file (s pathname :direction :input)
    (read-stream s (make-word-register))
    (hash-table-count *words*)))

(defun random-next (prev &optional (table *words*)) ;; NOTE optional just bc lazy
  (let* ((choices (gethash prev table))
         (i (random (reduce #'+ choices
                            :key #'cdr))))
    (dolist (pair choices)
      (if (minusp (decf i (cdr pair)))
          (return (car pair))))))

(defun generate-text (n) ; refactor
  (do ((curr (random-next '|.|) (random-next curr))
       (i 0 (1+ i)))
      ((= i n) n)
    (format t "~A " curr)))

;;; -----------------------------------------------------------------------------

(defun errno-henley-p (msg)
  (throw 'gotcha
    (concatenate 'string
                 msg ": this sentence was not written by Henley")))


(defun probability100 (event-occurs other-occur &key (key nil))
  (* (/ event-occurs (reduce #'+ other-occur :key key)) 100))

(defun make-words-memory ()
  (let ((prev '|.|)) ; Henley generates text starting from this symbol
    #'(lambda (word)
        (let* ((choices (gethash prev *words*)) ; words associated with `prev'
               ;; the frequency of a word that should have occurred after `prev'
               (word-freq (cdr (assoc word choices))))
          (if word-freq
              (progn
                (setf prev word) ; update `prev' to `word' when this exist...
                ;; ...and return the probability in percent of choosing `word'
                ;; between `choices'
                (utils:n-decimal-places 2 (probability100 word-freq choices
                                                    :key #'cdr)))
              ;; if the frequency did not exist, this association could not have
              ;; been produced by Henley
              (errno-henley-p
               (format nil "Impossible word sequence '~A' '~A'" prev word)))))))

;;; TODO better way of saving probabilities in `words-prob'
(defun henley-p (string)
  (catch 'gotcha ; transfer control when `string' is not `henley-p'
    (let ((buffer (make-string maxword))
          (pos 0)
          (is-possible-word (make-words-memory))
          ;; save probability of each word occurring in pair with the preceding
          ;; one for positive outputs
          (words-prob nil))
      (dotimes (i (length string) (values words-prob t))
        (let ((c (aref string i)))
          ;; the closure returned by `make-see' interns downcased symbols
          (cond ((upper-case-p c)
                 (errno-henley-p "Capital letter"))
                ((or (alpha-char-p c) (char= c #\'))
                 (progn
                   (setf (aref buffer pos) c)
                   (incf pos)))
                ((/= pos 0)
                 (let ((curr-word (subseq buffer 0 pos)))
                   ;; save the probability of observing this pair of words in
                   ;; case of a positive match
                   (push (cons curr-word
                               (funcall is-possible-word (intern curr-word)))
                         words-prob))
                 (setf pos 0)))
          (let ((p (punc c)))
            (when p
              (push (cons p (funcall is-possible-word p)) words-prob))))))))

(defun make-henleyp-test-bundle (text &optional type)
  (funcall (utils:make-hash-table-func 'remove) *words*)
  (format t "~A~%" type)
  (if (eq type 'filename)
      (read-text text)
      (with-input-from-string (s-stream text)
        (read-stream s-stream (make-word-register))))
  #'(lambda (arg &optional (func #'generate-text))
      (let ((test (cond ((numberp arg)
                         (with-output-to-string (*standard-output*)
                           (funcall func arg)))
                        ((stringp arg) (concatenate 'string arg " "))
                        (t "ERROR: give either a number & optional function to
                        generate text or some string of text already!"))))
        (format t "~A~%" test)
        (progn
          (format t "training input was:~%~A" text)
          (henley-p test)))))


;;; ex 8.6
(defparameter *forward-words* (make-hash-table :size 10000))

;;; Make another global table where to store, under each word in `*words*', the
;;; words that followed it along with their respective frequencies
;;; NOTE naive O(n^2) approach
(defun register-following-words ()
  (maphash #'(lambda (word _)
               (maphash #'(lambda (k following-words)
                            (let ((pair (assoc word following-words)))
                              (when pair
                                (push (cons k (cdr pair))
                                      (gethash word *forward-words*)))))
                        *words*))
           *words*))

(defun random-sentence-half (word n table)
  (let ((ret ""))
    (do ((curr (random-next (intern (string-downcase word)) table)
               (random-next curr table))
         (i 0 (1+ i)))
        ((>= i (floor (/ n 2))) ret)
      (setf ret (concatenate 'string ret (format nil "~A " curr))))))

;;; Generate a piece of random text with word given by the user in the middle.
;;; Do so by generating two sentences of length `(floor (/ n 2))', and using
;;; `word' in the middle of the concatenation of these two sentences. The two
;;; halves are produced in the same way: `random-sentence-half' is called once
;;; to produce the previous half (a chain of words that preceded each other in
;;; the original text, using `*words*'), and another time to produce the next
;;; half (chain of words that followed each other in the input, so using
;;; `*forward-words*'). In both cases, the chain starts with `word'.
(defun generate-text-with-word (word n)
  (let ((preceding-half (random-sentence-half word n *words*))
        (following-half (random-sentence-half word n *forward-words*)))
    (format t "~A~A ~A~%" preceding-half word following-half)))
