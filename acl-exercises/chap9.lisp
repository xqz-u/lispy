;; (in-package #:acl-chap-9)

;;; ex 9.1
(defun increasing-seq (lst)
  (apply #'<= lst))


;;; ex 9.2
;; NOTE ugly af experiment
;;; Final version, accepts an optional list of values to destructure `amount'
(defun make-change (amount &optional (components nil))
  "Compute the minimum number of `components' needed to reach `amount'"
  (let ((sorted-comps (if components
                          ;; sort to count grater components first
                          (sort components #'>)
                          '(25 10 5 1)))
        (ret nil))
    (dolist (comp sorted-comps (values-list ret)) ; return individual values
      (let ((n-comp (floor (/ amount comp))))
        ; save number for component and component itself
        (push (cons comp n-comp) ret)
        (decf amount (* n-comp comp))))))


;;; ex 9.3
(defun singing-comp-simulation (years)
  (let ((results (make-array years)))
    (dotimes (y years results)
      (let ((rand-score (random 10)))
        (setf (aref results y) (cons rand-score (- 10 rand-score)))))))


;;; ex 9.4
;;; NOTE a bit different to avoid passing too many parameters
(defstruct (point (:conc-name nil)
                  (:print-function (lambda (o str depth)
                                     (format str "#<(~A,~A)>" (x o) (y o)))))
  x y)

(defun delta (r0 r1 s0 s1 &key (type 'float))
  "General formula to compute change in a 2d domain from (r0,r1) -> (s0,s1)"
  (let ((res (/ (- r0 r1) (- s0 s1))))
    (if (eq type 'float)
        (float res)
        res)))

(defun slope-y-intercept (q0 q1)
  "Compute the slope and y-intercept of the line passing between `q0' and `q1'"
  (let* ((m (delta (y q1) (y q0) (x q1) (x q0)))
         (b (- (y q0) (* m (x q0)))))
    (cons m b))) ; return as single value since no information is accessory here

(defun lines-intersection (p0 p1 t0 t1)
  "Compute the coordinates of the intersection between the two lines `p0->p1'
   and `t0->t1' when the lines are not parallel"
  (destructuring-bind ((m0 . b0) (m1 . b1))
      (list (slope-y-intercept p0 p1) (slope-y-intercept t0 t1))
    (if (= m0 m1) ; slopes are equal so lines are parallel
        'parallel
        ;; when two lines y=ax+c and y'=bx+d intersect, y=y' -> ax+c=bx+d, so
        ;; x=(d-c)/(a-b)
        (let ((x-intersec (delta b1 b0 m0 m1)))
          (make-point :x x-intersec
                      ;; substitute the x just found in one of the line equations
                      :y (+ (* m0 x-intersec) b0))))))

(defun points-dist (e0 e1)
  "Euclidean distance between two points"
  (sqrt (+ (expt (- (x e0) (x e1)) 2)
           (expt (- (y e0) (y e1)) 2))))

;;; NOTE computationally cheaper way would be preferable
(defun is-point-on-segment (p l0 l1)
  "Return `p' if a point `p' lies on the segment `l0->l1', else nil"
  (unless (/= (+ (points-dist p l0) (points-dist p l1)) (points-dist l0 l1))
      p))

(defun segments-intersection (p0 p1 t0 t1)
  "Compute the coordinates of the intersection between the two segments `p0->p1'
   and `t0->t1', or return `parallel' or nil if no intersection exists"
  (let ((inter (lines-intersection p0 p1 t0 t1)))
    (if (eq inter 'parallel) ; no intersection can exist
        inter
        ;; intersection exists, but may be outside the span of the two segments
        (and (is-point-on-segment inter p0 p1)
             (is-point-on-segment inter t0 t1)))))


;;; ex 9.5
;; NOTE from http://www.shido.info/lisp/pacl2_e.html#numeric
;; TODO write my own version, can't understand this one
(defun  bisec (f min max epsilon)
  (let ((m (* 0.5 (+ min max))))
    (if (< (- max min) epsilon)
        m
        (let ((fmin (funcall f min))
              (fmax (funcall f max))
              (fm   (funcall f m)))
          (cond
            ((< 0 (* fmin fmax)) (error "wrong range"))
            ((= 0 fm) m)
            ((< 0 (* fmin fm)) (bisec f m max epsilon))
            ((< 0 (* fmax fm)) (bisec f min m epsilon))
            (t nil))))))


;;; ex 9.6
;;; NOTE check errata for number of args function should take
(defun horner-poly-eval (x &rest coeffs)
  "Evaluate a polynomial of degree n-1 using Horner's rule, given a value for the
   polynomial's variable `x' and a list of n coefficients"
  (labels ((inner (ns acc)
             (if ns
                 (inner (cdr ns) (+ (car ns) (* x acc)))
                 acc)))
    (cond ((null coeffs) x) ; degree 0/1?
          ((null (cdr coeffs)) (+ x (car coeffs))) ; degree 1
          (t (inner coeffs 0)))))


;;; ex 9.7
(log (1+ most-positive-fixnum) 2) ; -> 62.0 bits in SBCL
;; (log (1+ most-positive-word) 2) ; -> 64.0


;;; ex 9.8
most-positive-short-float ; 3.4028235e38
most-positive-single-float ; 3.4028235e38, equal to short?!
most-positive-double-float ; 1.7976931348623157d308
most-positive-long-float ; 1.7976931348623157d308, same?!
