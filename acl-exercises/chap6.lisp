;;; ex 1
(defun constituent (c)
  (and (graphic-char-p c)
       (not (char= c #\  ))))

(defun tokens-ext (str &key (test #'constituent) (start 0))
  (let ((p1 (position-if test str :start start)))
    (if p1
        (let ((p2 (position-if #'(lambda (c)
                                   (not (funcall test c)))
                               str :start p1)))
          (cons (subseq str p1 p2)
                (if p2
                    (tokens-ext str :test test :start p2)
                    nil)))
        nil)))


;;; ex 2
(defun finder-ext (obj vec start end key test)
  (let ((range (- end start)))
    (if (zerop range)
      (if (funcall test obj (funcall key (aref vec start)))
          obj
          nil)
      (let* ((mid (+ start (round (/ range 2))))
             (obj2 (funcall key (aref vec mid))))
        (cond ((< obj obj2) (finder-ext obj vec start (- mid 1) key test))
              ((> obj obj2) (finder-ext obj vec (+ mid 1) end key test))
              (t obj))))))

(defun bin-search-ext (obj vec &key
                             (key #'identity)
                             (test #'eql)
                             (start 0)
                             (end (1- (length vec))))
  (let ((len (length vec)))
    (and (not (zerop len))
         (finder-ext obj vec start end key test))))


;;; ex 3
(defun n-args (&rest args)
  (length args))


;;; ex 4
(defun 2most (fn lst)
  (cond ((null lst) (values nil nil))
        ((null (cdr lst)) (values (car lst) nil))
        (t (let* ((wins (car lst))
                  (sec-wins wins)
                  (max (funcall fn wins)))
             (dolist (obj (cdr lst))
               (let ((score (funcall fn obj)))
                 (when (> score max)
                   (setf sec-wins wins
                         wins obj
                         max score))))
             (values wins sec-wins)))))


;;; ex 5
(defun filter (fn lst)
  (let ((acc nil))
    (dolist (x lst)
      (let ((val (funcall fn x)))
        (if val (push val acc))))
    (nreverse acc)))

(defun remove-if-filter (fn lst)
  (filter #'(lambda (x)
              (unless (funcall fn x)
                x))
         lst))


;;; ex 6
(let ((max nil))
  (defun greatest-so-far (num)
    (if (or (null max) (> num max))
        (setf max num)
        max)))


;;; ex 7
(let ((num nil))
  (defun greater-than-num (curr)
    (let ((last num))
      (setf num curr)
      (cond ((null last) nil)
            ((> curr last) t)))))


;;; ex 8
(defun expensive (num) ; dummy function for the first ``frugal'' version
  (when (<= num 100)
    (* num num)))

;; NOTE bad solution: ``store'' will 'remember' useless arguments (> 100)
;; FIXME it is slightly wrong according to the spec: with already seen
;; arguments, it returns nil, and not the result of the previous same
;; computation (my misunderstanding)
(let ((store nil))
  (defun frugal (arg)
    (unless (equal store
                   (setf store (adjoin arg store)))
      (expensive arg))))

;; better solution from http://www.shido.info/lisp/pacl2_e.html#function,
;; runs in constant time and space
;; NOTE set ``:initial-element'' to ``nil'' or the function will fail
;; (defaults to 0)
(let ((store (make-array 101 :initial-element nil)))
  (defun frugal-b (n)
    (or (svref store n)
        (setf (svref store n) (expensive n)))))


;;; ex 9
(defun apply-octal (fn arg &rest arguments)
  (let ((*print-base* 8))
    (apply fn arg arguments)))

;; or simply... http://www.shido.info/lisp/pacl2_e.html#function
;; FIXME actually does not work with the ``apply'' syntax
(defun apply-octal-b (&rest apply-args)
  (let ((*print-base* 8))
    (apply #'apply apply-args)))
