;; ex 1
module module-switch-on-port-ne
;; (a b (c d))
(cons 'a (cons 'b (cons (cons 'c (cons 'd nil)) nil)))

;; (a (b (c (d))))
(cons 'a (cons (cons 'b (cons (cons 'c (cons (cons 'd nil) nil)) nil)) nil))

;; (((ab) c) d)
(cons (cons (cons 'a (cons 'b nil)) (cons 'c nil)) (cons 'd nil))

;; (a (b . c) . d)
(cons 'a (cons (cons 'b 'c) 'd))


;; ex 2
(defun new-union (list-1 list-2)
  (if list-2
      (let ((curr (car list-2)))
        (new-union
         (if (member curr list-1)
             list-1
             (append list-1 (cons curr nil)))
         (cdr list-2)))
      list-1))


;; ex 3
(defun incf-cdr-cell (a-cons)
  (cons (car a-cons)
        (+ (cdr a-cons) 1)))

(defun count-occurrence (l freqs)
  (if (null l)
      freqs
      (let* ((curr (car l))
             (curr-count (assoc curr freqs))
             (new-count
               (if (null curr-count)
                   (cons (cons curr 1) freqs)
                   ;; (rplacd curr-count (+ (cdr curr-count) 1)))))
                   (subst (incf-cdr-cell curr-count) curr-count freqs))))
        (count-occurrence (cdr l) new-count))))

(defun occurrences (l)
  (sort (count-occurrence l nil) #'> :key #'cdr))


;; ex 4
;; a)
(defun add-val (el value next)
  (cons (+ el value) next))

(defun pos+-help (l i)
  (if (null (cdr l))
      (add-val (car l) i nil)
      (let ((last-res (pos+-help (cdr l) (+ i 1))))
        (add-val (car l) i last-res))))

(defun pos+ (l)
  (pos+-help l 0))

;; b)
(defun pos+-iter (l)
  (let ((i 0)
        (acc nil))
    (dolist (ent l)
      (setf acc (cons (+ ent i) acc))
      (setf i (+ i 1)))
    acc))

;; c)
;; NOTE there is surely a simpler version than this zip-like approach...
(defun seq (start end)
  (if (>= start end)
      (cons end nil)
      (let ((res (seq (+ start 1) end)))
        (cons start res))))

(defun pos+-map (l)
  (mapcar (lambda (el)
     (+ (car el) (cadr el)))
   (mapcar #'list l (seq 0 (length l)))))

;; ... as these ones!
(defun pos+-map-1 (l)
  (let ((i 0))
    (mapcar #'(lambda (x)
              (let ((tmp (+ x i)))
                (incf i)
                tmp))
            l)))

(defun counter ()
  (let ((i -1))
    (lambda ()
      (incf i))))

;; prettiest?! Should compare performance...
(defun pos+-map-clos (l)
  (let ((cnt (counter)))
    (mapcar #'(lambda (el)
                (+ el (funcall cnt)))
            l)))

;; ex 6 (basically invert car & cdr for the following functions)
;; a)
(defun g-cons (a b)
  (cons b a))

;; b)
(defun g-list-rec (args)
  (if (null (cdr args))
      args
      (g-cons (g-list-rec (butlast args))
              (car (last args)))))

(defun g-list (&rest args)
  (if (atom args)
      (g-cons nil args)
      (g-list-rec args)))

(defun g-list-alt (&rest args) ; thx to &rest!!
  args)

;; c)
;; if the idea is to use the previously written funcs, I dont get it...
(defun g-length (l)
  (let ((cnt 1))
    (reduce #'(lambda (a b)
                (incf cnt))
            l)))

(defun g-length-norm (l)
  (if l
      (+ (g-length-norm (cdr l)) 1)
      0))

;; d)
(defun g-member-norm (item lst)
  (when lst
    (if (eql item (car lst))
        lst
        (g-member-norm item (cdr lst)))))


;; ex 7
(defun n-elts (elt n)
  (if (> n 1)
      (list n elt)
      elt))

(defun n-elts-mod (elt n)
  (if (> n 1)
      (cons n elt)
      elt))


;;; ex 8
;; FIXME buggy
(defun showdots (lst)
  (defun showdots-in (lst)
    (when lst
      (let ((inner (car lst)))
        (if (consp inner)
            (format nil "((~a . ~a) . NIL)"
                    (car inner)
                    (showdots-in (cdr inner)))
            (format nil "(~a . ~a)"
                    inner
                    (showdots-in (cdr lst)))))))
  (format t "~a" (showdots-in lst)))

;; Alternative (from http://www.shido.info/lisp/pacl2_e.html#list), simpler :/
(defun showdots-alt (ls)
  (defun showdots-rec (ls)
    (if ls
        (if (atom ls)
            ls
            (format nil "(~A . ~A)"
                    (showdots-rec (car ls))
                    (showdots-rec (cdr ls))))))
  (format t "~A" (showdots-rec ls)))


;; ex 9
(defparameter *network-1* '((a b c) (b a d) (c e) (d e f) (e a g)))

(defparameter *network* '((a b c) (b c) (c d)))

(defun longest-path (start end net)
  (bfs-l end (list (list start)) net nil))

(defun bfs-l (end queue net solution)
  (if (null queue)
      (reverse solution)
      (let* ((path (car queue))
             (node (car path)))
        (bfs-l end
             (append (cdr queue)
                     (new-paths-l path node net))
             net
             (if (eql node end)
                 path
                 solution)))))

(defun reachable-nodes (node net)
  (cdr (assoc node net)))

(defun new-paths-l (path node net)
  (let ((ret nil))
    (dolist (next-reach (reachable-nodes node net))
      (when (not (member next-reach path))
        (push (cons next-reach path) ret)))
    ret))

;; Elegant solution from http://www.shido.info/lisp/pacl2_e.html#list)
;; exploiting conditional evaluation!
(defun new-paths (path node net)
  (let (acc)
    (dolist (x (cdr (assoc node net)))
      (or (member x path)
          (push (cons x path) acc))
      acc))
