;;; ex 1
;; Yes it is, when they belong to different packages for example.

;;; ex 2
;; "FOO" the string: 3 bytes
;; FOO the symbol: only the name is 3 bytes, so overall it is heavier than
;; the string.

;;; ex 3

;;; ex 4
;;; a)
(defpackage "RING"
  :use "COMMON-LISP"
  :export "BUF" "BREF" "NEW-BUF" "BUF-INSERT" "BUF-POP" "BUF-NEXT" "BUF-RESET"
  "BUF-CLEAR" "BUF-FLUSH")

(in-package "RING")

;; and here follows the desired code

;;; b)
(defpackage "FILE"
  :use "COMMON-LISP" "RING")

(in-package "FILE")

;; and here follows the desired code
