;; ex 2
(defun 1-1 ()
  (cons 'a '(b c)))

(defun 1-2 ()
  (cons 'a (cons 'b '(c))))

(defun 1-3 ()
  (cons 'a (cons 'b (cons 'c nil))))


;; ex 3
(defun my-nth (l i)
  (and l ;; equal to
       (or (and (<= i 1) (car l))
           (my-nth (cdr l) (- i 1)))))

(defun fourth-1 (x)
  (my-nth x 4))

(defun fourth-2 (x)
  (car (cdr (cdr (cdr x)))))


;; ex 4
(defun my-max (a b)
  (if (< a b)
      a
      b))


;; ex 5
(defun enigma (x)
  "Check if x contains a nil"

  (and (not (null x))
       (or (null (car x))
           (enigma (cdr x)))))

(defun mystery (x y)
  "Find first index of x (not a list) in y"

  (if (null y)
      nil
      (if (eql (car y) x)
          0
          (let ((z (mystery x (cdr y))))
            (and z (+ z 1))))))


;; ex 7
(defun member-is-list (alist)
  (and alist
       (or (typep (car alist) 'list)
           (member-is-list (cdr alist)))))


;; ex 8
;; a)
(defun print-n-dots (n)
  (unless (<= n 0)
      (format t ".")
      (print-n-dots (- n 1))))

(defun print-n-dots-iter (n)
  (do ((i 0 (+ i 1)))
      ((>= i n))
    (format t ".")))


;; b)
(defun count-occurrence-helper (x alist acc)
  (if (null alist)
      acc
      (if (equal (car alist) x)
          (count-occurrence-helper x (cdr alist) (+ acc 1))
          (count-occurrence-helper x (cdr alist) acc))))

(defun count-occurrence-1 (x alist)
  (count-occurrence-helper x alist 0))

(defun count-occurrence (x alist)
  (if (null alist)
      0
      (let ((cnt (count-occurrence x (cdr alist))))
        (if (equal (car alist) x)
            (+ cnt 1)
            cnt))))


(defun count-occurrence-iter (x alist)
  (let ((cnt 0))
    (progn
      (dolist (el alist)
        (when (equal el x)
          (setf cnt (+ cnt 1)))))
    cnt))

;; ex 9
(defun summit-w (lst)
  (remove nil lst)
  (apply #'+ lst))

(defun summit-c (lst)
  (apply #'+
         (remove nil lst)))


;; b)
(defun summit-1-w (lst)
    (let ((x (car lst)))
      (if (null x)
          (summit (cdr lst))
          (+ x (summit (cdr lst))))))

(defun summit-1-c (lst)
  (if (null lst)
      0
      (let ((acc (summit-1-c (cdr lst))))
            (if (null (car lst))
                acc
                (+ acc (car lst))))))
