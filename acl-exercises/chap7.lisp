;;; ex 1 2
;; NOTE ``nreverse'' modifies the original arg (``reverse'' creates copy)
(defun list-file-content (filename &optional (read-func #'read-line))
  (with-open-file (in filename)
    (do ((line (funcall read-func in nil 'eof)
               (funcall read-func in nil 'eof))
         (acc nil (cons line acc)))
        ((eql line 'eof) (nreverse acc)))))


;;; ex 3
;; NOTE ugly solution :/
(defun commentless-file (file commentless-file
                         &optional (comment-sym #\%))
  (with-open-file (in file)
    (with-open-file (out commentless-file
                         :direction :output
                         :if-exists :supersede)
      (do ((curr (read-char in nil 'eof) (read-char in nil 'eof)))
          ((eql curr 'eof) 'eof)
        (if (not (char= curr comment-sym))
            (format out "~A" curr)
            (do ((_ curr  (read-char in nil 'eof)))
                ((or (eql _ 'eof) (char= _ #\Newline))
                 (terpri out))))))))


;;; ex 4
(defun pprint-2d-floats-matrix (m)
  (when (and (arrayp m)
            (= (array-rank m) 2))
    (let ((dims (array-dimensions m)))
      (dotimes (i (car dims))
        (dotimes (j (cadr dims))
          (format t "~10,2F " (aref m i j)))
        (terpri)))))


;;; ex 5
(defstruct buf
  vec (start -1) (used -1) (new -1) (end -1))

(defun bref (buf n)
  (svref (buf-vec buf)
         (mod n (length (buf-vec buf)))))

(defun (setf bref) (val buf n)
  (setf (svref (buf-vec buf)
               (mod n (length (buf-vec buf))))
        val))

(defun new-buf (len)
  (make-buf :vec (make-array len)))

(defun buf-insert (x b)
  (setf (bref b (incf (buf-end b))) x))

(defun buf-pop (b)
  (prog1
      (bref b (incf (buf-start b)))
    (setf (buf-used b) (buf-start b)
          (buf-new  b) (buf-end   b))))

(defun buf-next (b)
  (when (< (buf-used b) (buf-new b))
    (bref b (incf (buf-used b)))))

(defun buf-reset (b)
  (setf (buf-used b) (buf-start b)
        (buf-new  b) (buf-end   b)))

(defun buf-clear (b)
  (setf (buf-start b) -1 (buf-used  b) -1
        (buf-new   b) -1 (buf-end   b) -1))

(defun buf-flush (b str)
  (do ((i (1+ (buf-used b)) (1+ i)))
      ((> i (buf-end b)))
    (princ (bref b i) str)))

(defun stream-subst (old new in out)
  (let* ((pos 0)
         (len (length old))
         (buf (new-buf len))
         (from-buf nil))
    (do ((c (read-char in nil :eof)
            (or (setf from-buf (buf-next buf))
                (read-char in nil :eof))))
        ((eql c :eof))
      (cond ((or (char= c (char old pos))
                 (char= (char old pos) #\+)) ; '+' acting as wildcard
             (incf pos)
             (cond ((= pos len)            ; 3
                    (princ new out)
                    (setf pos 0)
                    (buf-clear buf))
                   ((not from-buf)         ; 2
                    (buf-insert c buf))))
            ((zerop pos)                   ; 1
             (princ c out)
             (when from-buf
               (buf-pop buf)
               (buf-reset buf)))
            (t                             ; 4
             (unless from-buf
               (buf-insert c buf))
             (princ (buf-pop buf) out)
             (buf-reset buf)
             (setf pos 0))))
    (buf-flush buf out)))

(defun file-subst (old new file1 file2)
  (with-open-file (in file1 :direction :input)
    (with-open-file (out file2 :direction :output
                               :if-exists :supersede)
      (stream-subst old new in out))))


;;; ex 6
;;; *d :: any digit
;;; *c :: any alphanumeric
;;; *a :: anything at all
;;; ** :: the '*' char
;;; TODO informative error messages in `abandon'
(defun parse-pattern (pattern)
  (let ((len (length pattern)))
    (labels ((abandon () ; error exit
               (throw 'abort (format nil "Malformed pattern -> ~A" pattern)))
             (inner (acc idx)
               (if (>= idx len)
                   ;; base case, reverse and vectorize the pattern
                   ;; so that it can use index logic
                   (concatenate 'vector (nreverse acc))
                   (let ((curr-c (char pattern idx)))
                     (if (char= curr-c #\*)
                         (inner (cons
                                 (if (< idx (1- len))
                                     (case (char pattern (1+ idx))
                                       (#\d 'num)
                                       (#\c 'alphanum)
                                       (#\a 'kstar)
                                       (#\* #\*)
                                       ;; the control symbol was found, but a
                                       ;; wrong format character followed
                                       (t (abandon)))
                                     ;; control symbol at the end of the pattern
                                     (abandon))
                                 acc) (+ idx 2))
                         (inner (cons curr-c acc) (1+ idx)))))))
      (inner nil 0))))

(defun stream-subst (pattern new in out)
  (catch 'abort
    (let* ((pos 0)
           (old (parse-pattern pattern)) ; parse the pattern
           (len (length old)) ; #pattern /= #old!
           (buf (new-buf len))
           (from-buf nil))
      (do ((c (read-char in nil :eof)
              (or (setf from-buf (buf-next buf))
                  (read-char in nil :eof)))
           (p (svref old pos) ; the current element in the pattern
              (svref old pos)))
          ((eql c :eof))
        (cond ((or (and (eq p 'num) (digit-char-p c)) ; new accepting conditions
                   (and (eq p 'alphanum) (alphanumericp c))
                   (eq p 'kstar)
                   (and (characterp p) (char= c p)))
               (incf pos)
               (cond ((= pos len)            ; 3
                      (princ new out)
                      (setf pos 0)
                      (buf-clear buf))
                     ((not from-buf)         ; 2
                      (buf-insert c buf))))
              ((zerop pos)                   ; 1
               (princ c out)
               (when from-buf
                 (buf-pop buf)
                 (buf-reset buf)))
              (t                             ; 4
               (unless from-buf
                 (buf-insert c buf))
               (princ (buf-pop buf) out)
               (buf-reset buf)
               (setf pos 0))))
      (buf-flush buf out))))
