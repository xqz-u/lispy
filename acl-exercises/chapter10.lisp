;; (in-package #:acl-chap-10)

;;; ex 1
(defun ex1 ()
  (destructuring-bind (x y z) '(a b (c d))
    (values `(,z ,x z)          ; a
            `(x ,y ,@z)         ; b
            `((,@z ,x) z))))    ; c


;;; ex 2
(defmacro cif (condition then &optional else)
  `(cond (,condition ,then)
         (t ,else)))


;;; ex 3
;; NOTE this will fail since nth evaluates ALL its arguments
(defun nth-expr-w (n &rest l)
  (nth (1- n) l))

;; NOTE works for simple cases, but not when `n' refers to a variable in a
;; lexical context due to the call to `eval'
(defmacro nth-expr% (n &rest lst)
  (let ((ng (gensym)))
    `(let ((,ng ,n))
       (if  (> ,ng ,(length lst))
            "not enough expressions"
            (last (subseq ',lst 0 ,ng))))))


(defun test ()
  (destructuring-bind (n x y) '(3 "lose" "win")
    (nth-expr% n
              (format t "You ~S!" x)
              (format t "You ~S!" x)
              (format t "You moron ~S!" y)
              (format t "You ~S!" x))))
