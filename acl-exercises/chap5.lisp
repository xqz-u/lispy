;;; ex 1
;;; a)
(setq y '(4 5 6))

(let ((x (car y)))
  (cons x x))

((lambda (p) (cons p p))
 (car y))

;;; b)
(setq x '(1 2 3) z 19)

(let* ((w (car x))
       (y (+ w z)))
  (cons w y))

((lambda (w)
   ((lambda (p)
      (cons w p))
    (+ w z)))
 (car x))


;;; ex 2
(defun mystery (x y)
  (cond ((null y) nil)
        ((eql (car y) x) 0)
        (t (let ((z (mystery x (cdr y))))
             (cond (z (+ z 1))
                   (t nil))))))


;;; ex 3
(defun square+5 (x)
  (if (and (numberp x)
           (> x 5))
      (* x x)
      x))


;;; ex 4
(defconstant month
  #(0 31 59 90 120 151 181 212 243 273 304 334 365))

(defun leap? (y)
  (and (zerop (mod y 4))
       (or (zerop (mod y 400))
           (not (zerop (mod y 100))))))

(defun month-num (m y)
  (+ (svref month (- m 1))
     (if (and (> m 2) (leap? y)) 1 0)))

(defun month-num-case (m y)
  (+ (case m
       (1 0)
       (2 31)
       (3 59)
       (4 90)
       (5 120)
       (6 151)
       (7 181)
       (8 212)
       (9 243)
       (10 273)
       (11 304)
       (12 334)
       (13 365))
     (if (and (> m 2) (leap? y)) 1 0)))


;;; ex 5
;;; NOTE the parameter ``obj'' is a single array element, not a
;;; subsequence of length > 1 of array elements

;; NOTE not calling ``member'' at each loop iteration and calling
;; ``remove-duplicates'' once at the end has better time complexity
(defun precedes-iter (obj vec)
  (when (arrayp vec)
    (let ((acc nil)
          (i 0))
      (do ((i i (1+ i))
           (oldi i i))
          ((>= i (length vec)))
        (let ((curr (aref vec i))
              (prev (aref vec oldi)))
          (when (and (eql curr obj)
                     (> i 0))
            (push prev acc))))
      (remove-duplicates (reverse acc)))))

;; NOTE could improve, even in performance
(defun precedes (obj vec)
  (defun precedes-in (obj vec i acc)
    (when (< i (1- (length vec)))
        (let ((acc (precedes-in obj vec (1+ i) acc)))
          (if (and (> i 0)
                   (eql (aref vec i) obj))
              (cons (aref vec (1- i)) acc)
              acc))))
  (when (arrayp vec)
    (remove-duplicates (precedes-in obj vec 0 nil))))


;;; ex 6
(defun intersperse-iter (obj lst)
  (let ((acc nil))
    (dotimes (i (length lst))
      (push (elt lst i) acc)
      (unless (= i (1- (length lst)))
        (push obj acc)))
    (reverse acc)))

(defun intersperse (obj lst)
  (defun intersperse-in (obj lst acc)
    (when lst
      (let ((acc (intersperse-in obj (cdr lst) acc)))
        (cons obj (cons (car lst) acc)))))
  (cdr (intersperse-in obj lst nil)))


;;; ex 7
;; a)
(defun diff1-rec (lst)
  (cond ((null (cdr lst)) t)
        (t (and (= 1 (- (first lst) (second lst)))
                (diff1-rec (cdr lst))))))

;; b)
(defun diff1-do (lst)
    (do ((step lst (cdr step)))
        ((null (cdr step)) t)
      (unless (= 1 (- (first step) (second step)))
        (return nil))))

;; c)
(defun diff1-mapc (lst)
  (block nil             ; can avoid & write ``return-from diff1-mapc''
    (when (mapc #'(lambda (f s)
                    (unless (= 1 (- f s))
                      (return nil)))
                lst
                (cdr lst))
      t)))


;;; ex 8
;; NOTE not tail recursive :/ could improve by having ``mmin'' and
;; ``mmax'' as arguments & update them at each call
(defun range (vec)
  (defun range-in (vec i)
    (let ((this (aref vec i)))
      (if (= (length vec) (1+ i))
          (values this this)
          (multiple-value-bind (mmin mmax) (range-in vec (1+ i))
            (cond ((< this mmin) (values this mmax))
                  ((> this mmax) (values mmin this))
                  (t (values mmin mmax)))))))
  (when (vectorp vec)
    (range-in vec 0)))


;;; ex 9
(defvar *network-1* '((a b c) (b a d) (c e) (d e f) (e a g)))
(defvar *network* '((a b c) (b c) (c d)))

;; a)
(defun shortest-path (start end net)
  (catch 'end
    (bfs end (list (list start)) net)))

(defun bfs (end queue net)
  (when queue
    (let* ((path (car queue))
           (node (car path)))
      (bfs end
           (append (cdr queue)
                   (new-paths path node net end))
           net))))

(defun new-paths (path node net end)
  (mapcar #'(lambda (n)
              (let ((newp (cons n path)))
                (if (eql n end)
                    (throw 'end (reverse newp))
                    newp)))
          (cdr (assoc node net))))

;; b)

(defun shortest-path (start end net)
  (bfs end (list (list start)) net))

(defun bfs (end queue net)
  (when queue
    (let* ((path (car queue))
           (node (car path)))
      (multiple-value-bind (new-path check)
          (new-paths path node net end)
        (if (eql check 'found)
            new-path
            (bfs end
                 (append (cdr queue) new-path)
                 net))))))

(defun new-paths (path node net end)
  (mapcar #'(lambda (n)
              (let ((newp (cons n path)))
                (if (eql n end)
                    (return-from new-paths
                      (values (reverse newp) 'found))
                    newp)))
          (cdr (assoc node net))))
