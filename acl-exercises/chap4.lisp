(setf *print-array* t)

(defparameter test-marr
  (make-array '(3 3) :initial-contents '((a b c) (d e f) (g h i))))

(defparameter test-arr #2A((a b) (c d)))

;; Equivalent to test-sarr definition, 3x3 matrix with rank 2
(defparameter test-arr-1 #2A((1 2 3) (4 5 6) (7 8 9)))

(defparameter test-arr-2 #2A((1 2 3 4) (5 6 7 8) (9 10 11 12) (13 14 15 16)))

;; This would be a VECTOR of RANK 1 (array-dimensions = 3 aka the length of
;; the vector)
(defparameter test-arr-vec #3((1 2 3) (4 5 6) (7 8 9)))

(defun pprint-2d-matrix (m)
  "Pretty print a two-dimensional matrix"

  (let ((dims (array-dimensions m)))
    (dotimes (i (car dims))
      (dotimes (j (cadr dims))
        (format t "~A " (aref m i j)))
      (format t "~%"))))

;;; ex 1
(defun quarter-turn (m)
  "Rotate a 2dim square matrix 90 degrees clockwise"

  (when (arrayp m)
    (let* ((dims (array-dimensions m))
           (nrows (car dims)))
      (if (not (and (= 2 (length dims))
                    (= nrows (cadr dims))))
          "Give a 2dim NxN matrix!"
          (let ((rot-m (make-array (list nrows nrows))))
            (dotimes (i nrows)
              (dotimes (j nrows)
                (setf (aref rot-m i j)
                      (aref m (- (1- nrows) j) i))))
            rot-m)))))


;;; ex 2
(defun copy-list-reduce (lst)
  "Copy a list using ``reduce''"

  (reduce #'cons lst :from-end t :initial-value nil))

(defun reverse-reduce (lst)
  "Reverse a list using ``reduce''"

  (reduce (lambda (x y)
            (cons y x))
          lst :initial-value nil))


;;; ex 3
(defstruct (3node (:print-object
                   (lambda (inst stream)
                     (format stream "#<~A>" (3node-elt inst)))))
  elt (child-1 nil) (child-2 nil) (child-3 nil))

(defvar 3tree-ex
  (make-3node :elt "The root"
              :child-1 (make-3node :elt "first child")
              :child-2 (make-3node :elt '(second child))
              :child-3 (make-3node :elt 7568)))

;;; a)
(defun copy-object-eql (obj)
  "Copy some object s.t. (eql x obj) would return true.
   NOTE not copying atoms"

  (if (listp obj)
      (copy-list obj)
      (if (arrayp obj)
          (copy-seq obj)
          obj)))

;; NOTE (eql (3node-elt original-t) (3node-elt copy-t)) won't return true for
;; atomic :elt :/
(defun copy-3tree (3tree)
  "Recursively copy a tree with up to 3 children per node s.t. no node in
   the new tree is eql to a node in the original tree"

  (when 3tree
    (make-3node :elt (copy-object-eql (3node-elt 3tree))
                :child-1 (copy-3tree (3node-child-1 3tree))
                :child-2 (copy-3tree (3node-child-2 3tree))
                :child-3 (copy-3tree (3node-child-3 3tree)))))

;; b)
(defun find-object-3tree (3tree object &key (test #'eql))
  "Return T if a given object is stored in a 3node tree. It is possible
   to specify the equality predicate used as test with the ``:test'' key
   (defaults to ``eql'')"

  (when 3tree
        (or (funcall test (3node-elt 3tree) object)
            (find-object-3tree (3node-child-1 3tree) object)
            (find-object-3tree (3node-child-2 3tree) object)
            (find-object-3tree (3node-child-3 3tree) object))))


;;; ex 4
(defstruct (node (:print-object
                  (lambda (n s)
                    (format s "#<~A>" (node-elt n)))))
  elt (l nil) (r nil))

(defvar simple-bst (make-node :elt 2
                              :l (make-node :elt 1)
                              :r (make-node :elt 3)))

(defvar bst-ex
  (make-node :elt 8
             :l (make-node :elt 5
                           :l (make-node :elt 4)
                           :r (make-node :elt 6))
             :r (make-node :elt 10
                           :r (make-node :elt 12))))

(defun print-dec-bst (bst)
  "Print binary search tree in decreasing order"

  (when bst
    (print-dec-bst (node-r bst))
    (format t "~A " (node-elt bst))
    (print-dec-bst (node-l bst))))

;;; My closest attempt...
(defun list-bst-nw (bst)
  (when bst
    (cons (list-bst-nw (node-r bst))
          (cons (node-elt bst)
                (list-bst-nw (node-l bst))))))

;;; Working solution from http://www.shido.info/lisp/pacl2_e.html#data
(defun list-bst-decreasing (bst)
  "Return the list representation of a binary search tree in decreasing order"

  (defun inner (bst acc)
    (if bst
        (inner (node-r bst) (cons (node-elt bst) (inner (node-l bst) acc)))
        acc))
  (inner bst nil))



;;; ex 5
;; ERRATA http://www.paulgraham.com/ancomliser.html


;;; ex 6
;;; a)
(defparameter test-alist '((first . 6) (second . mimmo) (third . 890)))

(defun alist-hash-table (alist)
  "Turn an assoc list into a hash table recursively"

  (defun inner (alist ht)
    (when alist
      (push (cdar alist) (gethash (caar alist) ht))
      (inner (cdr alist) ht)
      ht))
  (inner alist (make-hash-table)))

(defun alist-hash-table-iter (alist)
  "Turn an assoc list into a hash table iteratively"

  (let ((ht (make-hash-table)))
    (dolist (kv alist)
      (push (cdr kv) (gethash (car kv) ht)))
    ht))

;;; b)
;;; FIXME the tuple which is arg to maphash is not consed to a dotted list!
(defun hash-table-alist (ht)
  "Turn a hash table into an assoc list using ``maphash''"

  (let ((acc nil))
    (maphash #'(lambda (k v)
                 (push (cons k v) acc))
             ht)
    acc))
