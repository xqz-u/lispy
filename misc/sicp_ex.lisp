;;; mind blowing!!
(defun cons% (a b)
  #'(lambda (f)
      (funcall f a b)))

(defun car% (x)
  (funcall x #'(lambda (p q)
                 p)))

(defun cdr% (x)
  (funcall x #'(lambda (p q)
                 q)))


;;; ex 2.4
(defun cons% (a b)
  #'(lambda (f)
      (funcall f a b)))

(defun car% (s)
  (funcall s #'(lambda (c cc)
                 (expt 2 c))))

(defun cdr% (s)
  (funcall s #'(lambda (c cc)
                 (expt 3 cc))))


;;; ex 2.6
;; ???
(defun zero ()
  #'(lambda (f)
      #'(lambda (x)
          x)))

(defun add-1 (n)
  #'(lambda (f)
      #'(lambda (x)
          (funcall f (funcall (funcall n f) x)))))
