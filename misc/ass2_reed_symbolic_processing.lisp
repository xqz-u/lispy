;;; TASK-A: Graph to alist
(defparameter *graph1* '((1 2 10)
                         (1 3 4)
                         (1 5 15)
                         (2 3 8)
                         (2 4 9)
                         (4 5 8)))

(defun register-node (alist node-refs-list start end dist)
  (cond ((null node-refs-list)
         (setf alist (cons (list start (list end dist))
                           alist)))
        (t (setf alist (subst (append node-refs-list
                                      (list (list end dist)))
                              node-refs-list alist :test #'equal)))))

;; TODO express with higher-order functional programming?!
(defun expand-alist (from to dist alist)
  (register-node (register-node alist
                                (assoc from alist)
                                from to dist)
                 (assoc to alist)
                 to from dist))

(defun graph-to-alist (graph)
  (labels ((rec-helper (graph alist)
             (if graph
                 (destructuring-bind (from to dist) (car graph)
                   (rec-helper (cdr graph)
                               (expand-alist from to dist alist)))
                 (reverse alist))))
    (rec-helper graph nil)))


;;; TASK-B: Simple depth-first search
(defun node-neighbors (node graph-alist)
  (cdr (assoc node graph-alist)))

;; NOTE ugly and not functional :/
(defun dfs-helper (curr goal visited tot-cost graph-alist)
  (let ((neighbors (node-neighbors curr graph-alist)))
    (when neighbors
      (setf visited (adjoin curr visited))
      (if (eql curr goal)
          tot-cost
          (dolist (next-route neighbors)
            (let ((next-node (car next-route))
                  (cost (cadr next-route)))
              (unless (member next-node visited)
                (let ((res (dfs-helper next-node goal visited
                                       (+ tot-cost cost)
                                       graph-alist)))
                  (when res
                    (return res))))))))))

(defun dfs (start end graph-alist)
  (dfs-helper start end nil 0 graph-alist))
