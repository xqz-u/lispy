(defstruct (card (:conc-name nil)
                 (:print-function (lambda (o s d)
                                    (format s "#[~A]<~A>"
                                            (strength o)
                                            (category o)))))
  strength
  (category nil))

(defvar *strength-values*
  '((|Wisdom and Knowledge| .
     (creativity curiosity judgment love of learning perspective))
    (|Courage| . (bravery perseverance honesty zest))
    (|Humanity| . (love kindness social intelligence))
    (|Justice| . (teamwork fairness leadership))
    (|Temperance| . (forgiveness humility prudence self-regulation))
    (|Transcendence| .
     (|appreciation of beauty and excellence| gratitude hope humor
      spirituality))))

(defun make-deck (cards-values)
  (mapcar #'(lambda (values)
              (mapcar #'(lambda (categoris)
                          ))
              (make-card val))
          cards-values))
