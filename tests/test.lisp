;; TODO reformat
(defun dot-product-helper (list_a list_b acc)
  (if (null (or list_a list_b))
      acc
      (dot-product-helper
       (cdr list_a)
       (cdr list_b)
       (+ acc
          (* (car list_a)
             (car list_b))))))


(defun dot-product (list_a list_b)
  "Perform the dot product between two vectors of equal length"
  (if (/= (length list_a) (length list_b))
      (format t "Give two lists of equal size")
      (dot-product-helper list_a list_b 0)))


;; CLOSURE EXAMPLE!
(defun n-multiplier (n)
  (lambda (y) (* y n)))

(defvar 4-multiplier
  (n-multiplier 4))

(funcall 4-multiplier 10)
;;;;;;;;;;;;;;;;;;;;;;;;;

(defun mul (seq)
  (reduce #'(lambda (x y)
              (* x y))
          seq))

(defun mul-1 (seq)
  (reduce #'* seq))

(defun counter-class ()
  (let ((counter 0))
    (lambda () (incf counter))))

(defun my-list (&rest args)
  (let ((head (car args))
        (tail (cdr args)))
    (if (and (= (length args) 1)
             (atom head))
        (cons head nil)
        (if (null tail)
            head
            (cons head (my-list tail))))))

(defun my-reverse (l)
  (if (null l)
      nil
      (cons
       (car (last l))
       (my-reverse (butlast l)))))

(defun n-list-1 (&rest args)
  (reverse (my-list args)))
