(defpackage #:dummy-lib
  (:use #:common-lisp)
  (:export #:dummy #:another-dummy))

(defpackage #:dummy-tests
  (:use #:common-lisp #:dummy-lib)
  (:export #:my-dummy-tester))
