(in-package #:dummy-tests)

(defun my-dummy-tester ()
  (dummy 1)
  (another-dummy 1 2 3 4)
  (dummy-lib:another-dummy "with : prefix"))
