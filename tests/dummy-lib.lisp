(in-package #:dummy-lib)

(defun dummy (param)
  (1+ param))

(defun another-dummy (&rest args)
  (format t "~A~%" args))
