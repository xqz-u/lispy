;;;; using #: read macro as in https://courses.cs.northwestern.edu/325/readings/packages.php

(defpackage #:utils
  (:use #:common-lisp)
  (:export #:alist-hash-table #:hash-table-alist #:all #:subsets
           #:cartesian-product #:cart-prod #:permutations #:make-hash-table-func
           #:n-decimal-places))

;; package for exercise 8.5 FIXME problem with `use-package' and symbols interned
;; in this package
(defpackage #:henley
  (:use #:common-lisp #:utils)
  (:export #:make-henleyp-test-bundle #:henley-p #:read-text #:read-stream))

(defpackage #:acl-chap-9
  (:use #:common-lisp #:utils)
  (:export #:increasing-seq #:in-cents #:singing-comp-simulation
           #:segments-intersection))

(defpackage #:acl-chap-10
  (:use #:common-lisp)
  (:export #:ex1))
