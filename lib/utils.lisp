(in-package #:utils)


(defun alist-hash-table (alist)
  "Turn an assoc list into a hash table iteratively"
  (let ((ht (make-hash-table)))
    (dolist (kv alist)
      (push (cdr kv) (gethash (car kv) ht)))
    ht))


(defun nats (&optional (n 1))
  "Return a function which when called returns the next natural number,
   starting from n"
  (let ((mem nil))
    (lambda ()
      (if mem
          (incf mem)
          (setf mem n)))))


(defun all (pred list &key (initial-value t))
  "Short circuited version of `reduce' on a list with a predicate -> bool"
  (labels ((inner (list acc)
             (if (or (null list) (null acc))
                 acc
                 (inner (cdr list) (and acc (funcall pred (car list)))))))
    (inner list initial-value)))


;;; TODO use arrays for faster runs of `all' (would not be reversed!)
(defun sieve (n)
  "The first n primes, computed with the Sieve of Eratosthenes method"
  (let ((naturals (nats 2)))
    (labels ((inner (nat acc len)
               (if (= len n)
                   (nreverse acc)
                   (multiple-value-call #'inner
                     (funcall naturals)
                     (if (all (lambda (x)
                                   (not (zerop (mod nat x))))
                                 acc)
                         (values (cons nat acc) (1+ len))
                         (values acc len))))))
      (inner (funcall naturals) nil 0))))


(defun subsets (choices len)
  "Return all possible subsets of length `len' from the `choices' set"
  (labels ((inner (curr curr-len)
             (if (= curr-len len)
                 (list curr) ; base case, add the combination to the list
                 ;; otherwise, concatenate all the next possible combinations
                 (reduce #'nconc
                         (mapcar #'(lambda (x)
                                     (inner (cons x curr) (1+ curr-len)))
                                 choices)))))
    (inner nil 0)))


(defun cartesian-product (a b)
  "Perform the Cartesian product between set a and set b"
  (labels ((inner (set1 store)
             (if (null set1)
                 store
                 (inner (cdr set1)
                        (append (mapcar #'(lambda (x)
                                            (list (car set1) x))
                                        b)
                                store)))))
    (inner a nil)))


(defun cart-prod (lists)
  "Perform the Cartesian product between n `lists', represented as a list of
   lists"
  (if (= (length lists) 2)
      (cartesian-product (car lists) (cadr lists))
      (mapcan #'(lambda (new)
                  (mapcar #'(lambda (partial)
                              (cons new partial))
                          (cart-prod (cdr lists))))
              (car lists))))


;;; NOTE from Norvig, Peter "Paradigms of Artificial Intelligence Programming"
;;; Morgan Kaufmann Publishers, Inc, San Francisco, 1992, pp 766-768.
(defun permutations (bag)
  "Return a list of all the permutations of the input."
  ;; If the input is nil, there is only one permutation:
  ;; nil itself
  (if (null bag)
      '(())
      ;; Otherwise, take an element, e, out of the bag.
      ;; Generate all permutations of the remaining elements,
      ;; And add e to the front of each of these.
      ;; Do this for all possible e to generate all permutations.
      (mapcan #'(lambda (e)
                  (mapcar #'(lambda (p) (cons e p))
                          (permutations
                           (remove e bag :count 1 :test #'eq))))
              bag)))


(defun hash-table-func (func table)
  "General template to apply a function `func' to the hash table `table'"
    (maphash #'(lambda (k v)
                 (funcall func k v))
             table))


;;; TODO add as 'alist to `make-hash-table-function'. Need a way to link the
;;; accumulator referenced in the lambda passed to `maphash' to the same
;;; accumulator declared in the lexical scope of `hash-table-func'
(defun hash-table-alist (ht)
  "Turn a hash table into an assoc list using ``maphash''"
  (let ((acc nil))
    (maphash #'(lambda (k v)
                 (push (cons k v) acc))
             ht)
    acc))


(defun make-hash-table-func (action)
  "Return a function that performs `action' on an arbitrary hash table.
  Only removal and printing (the default) are supported atm"
  #'(lambda (table)
      (hash-table-func (case action
                         ('remove #'(lambda (k _) (remhash k table)))
                         ;; ('alist #'(lambda (k v) (push (cons k v) store)))
                         (t #'(lambda (k v) (format t "~A ~A~%" k v))))
                       table)))


(defun n-decimal-places (n arg &optional (stream nil))
  (format stream "~,vf" n arg))


;;; TODO `flatten' to a given depth,
;;; e.g. (flatten '(((1 0) (0 1))) 2) => ((1 0) (0 1))
